import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import es from 'vuetify/es5/locale/es';

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    locales: { es },
    current: 'es',
  },
  icons: {
    iconfont: 'fa',
  },
  theme: {
    themes: {
      light: {
        primary: '#424242',
        secondary: '#ff5252',
        accent: '#f5f5f5',
        error: '#b71c1c',
      },
      dark: {
        primary: '#424242',
        secondary: '#ff5252',
        accent: '#f5f5f5',
        error: '#b71c1c',
      },
    },
    options: {
      customProperties: true,
    },
  },
});

